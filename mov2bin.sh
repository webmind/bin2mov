#!/bin/bash
INFILE=$1
OUTFILE=$2
PREFIX="tmp1312-"
POSTFIX="bin"
IMGTYPE="png"
TMPDIR=`mktemp -d`
echo "Converting $INFILE to $IMGTYPE in $TMPDIR"
ffmpeg -v quiet -i "$INFILE" ${TMPDIR}/$PREFIX%03d.$IMGTYPE
echo -n "Decoding qr-codes"
for I in ${TMPDIR}/$PREFIX*.$IMGTYPE; do 
    zbarimg -q -Sdisable -Sqrcode.enable --raw $I | base64 -d > $I.$POSTFIX; 
    echo -n .
done
echo; echo "Concatinating into $OUTFILE"
ls ${TMPDIR}/$PREFIX*.$IMGTYPE.$POSTFIX |sort | xargs cat > $OUTFILE
rm -rf -- "$TMPDIR"
