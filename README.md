# bin2mov

Simple tool to turn binaries into movies and back.

These two tools convert binaries into a movie of QR-codes. This doesn't work with every format, mkv/mp4/avi work well, webm not really and ogv fails horribly.

bin2mov.sh depends on: (most you find on any system)
* bash
* split
* base64
* mktemp
* expr
* echo 
* stat
* qrencode
* ffmpeg

On debian, just run 'apt install qrencode ffmpeg' and you're set.

mov2bin.sh depends on: (same story)
* bash
* mktemp
* echo
* ls
* sort
* xargs
* cat
* base64
* ffmpeg
* zbarimg

On debian, just run 'apt install ffmpeg zbar-tools' and you're set.

The defaults work fairly well for the above mentioned formats, but feel free to tweak the variables if you run into issues. Decreasing the frame/block size and/or increasing the dot size might help if you run into issues.

Code is provided AS IS, no warranty on it either working or not breaking your stuff, it just is.
