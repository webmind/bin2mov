#!/bin/bash

INFILE=$1
OUTFILE=$2
INSIZE=`stat --printf %s "$INFILE"`
BSIZE=1312
IMGTYPE="png"
DOTSIZE=5
PREFIX="pre"
BCOUNT=`expr $INSIZE / $BSIZE`
SUFLEN=`echo -n $BCOUNT | wc -c`
TMPDIR=`mktemp -d`
echo "Splitting $INFILE in $BCOUNT blocks in $TMPDIR"
split -d -a $SUFLEN -b $BSIZE "$INFILE" $TMPDIR/$PREFIX
echo "Encoding $BCOUNT blocks to qrcodes in $IMGTYPE with dotsize $DOTSIZE"
for I in "$TMPDIR/$PREFIX"*; do 
    base64 $I | qrencode -s $DOTSIZE -t $IMGTYPE -8 -o $I.$IMGTYPE; 
    echo -n "."
done
echo "done.";
echo "Converting $BCOUNT qrcodes to video file $OUTFILE"
ffmpeg -f image2 -v quiet -framerate 12 -i $TMPDIR/${PREFIX}%0${SUFLEN}d.$IMGTYPE "$OUTFILE"

rm -rf -- "$TMPDIR"
